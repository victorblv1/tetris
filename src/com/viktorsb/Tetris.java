/**
 * Created by viktors belovs
 * October 17th 2018
 * Tools: IntelliJ IDEA Ultimate
 */

package com.viktorsb;

import com.viktorsb.Board;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Tetris extends JFrame {

    JLabel statusbar;

    public Tetris() {

        statusbar = new JLabel(" 0");
        add(statusbar, BorderLayout.SOUTH);
        Board board = new Board(this);
        add(board);
        board.start();

        setSize(200, 400);
        setTitle("tetRis");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public JLabel getStatusbar() {
        return statusbar;
    }

    public static void main(String[] args) {

        Tetris game = new Tetris();
        game.setLocationRelativeTo(null);
        game.setVisible(true);
    }
}
